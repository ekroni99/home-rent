<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table='comments';
    public function customers(){
        return $this->belongsTo(Customer::class,'customer_id');
    }
    public function posts(){
        return $this->belongsTo(Post::class,'post_id');
    }
    public function replies(){
        return $this->hasMany(Reply::class,'comment_id');
    }
}
