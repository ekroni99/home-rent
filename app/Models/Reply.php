<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    protected $table='replies';
    
    public function customers(){
        return $this->belongsTo(Customer::class,'customer_id');
    }
    public function comments(){
        return $this->belongsTo(Comment::class,'comment_id');
    }
}
