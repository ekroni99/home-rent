<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table='customers';
    
    public function role(){
        return $this->belongsTo(Role::class,'role_id');
    }
    public function posts(){
        return $this->hasMany(Post::class,'customer_id');
    }
    public function comments(){
        return $this->hasMany(Comment::class,'customer_id');
    }
    public function replies(){
        return $this->hasMany(Reply::class,'customer_id');
    }
}
