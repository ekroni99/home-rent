<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table='posts';

    public function customers(){
        return $this->belongsTo(Customer::class,'customer_id');
    }
    public function comments(){
        return $this->hasMany(Comment::class,'post_id');
    }
}
