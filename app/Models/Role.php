<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table='roles';

    public function customers(){
        return $this->hasMany(Customer::class,'role_id');
    }
}
