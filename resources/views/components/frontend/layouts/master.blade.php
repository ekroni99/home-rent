<!DOCTYPE html>
<html lang="en">
    <x-frontend.layouts.partials.head/>
<body>
    <div class="container-xxl bg-white p-0">
        <!-- Spinner Start -->

        <!-- Spinner End -->


        <!-- Navbar Start -->
        <x-frontend.layouts.partials.navbar/>
        <!-- Navbar End -->


        <!-- Header Start -->
        <x-frontend.layouts.partials.header/>
        <!-- Header End -->


        <!-- Search Start -->
        <x-frontend.layouts.partials.search/>
        <!-- Search End -->


        <!-- Category Start -->
        {{-- <x-frontend.layouts.partials.category/> --}}
        <!-- Category End -->


        <!-- About Start -->
        {{-- <x-frontend.layouts.partials.about/> --}}
        <!-- About End -->


        <!-- Property List Start -->
        <x-frontend.layouts.partials.propertylist/>
        <!-- Property List End -->


        <!-- Call to Action Start -->
        <x-frontend.layouts.partials.callaction/>
        <!-- Call to Action End -->


        <!-- Team Start -->
        <x-frontend.layouts.partials.teamstart/>
        <!-- Team End -->


        <!-- Testimonial Start -->
        <x-frontend.layouts.partials.testimonial/>
        <!-- Testimonial End -->
        

        <!-- Footer Start -->
        <x-frontend.layouts.partials.footer/>
        <!-- Footer End -->


        <!-- Back to Top -->
        <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>
    </div>

    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="{{ asset('ui/frontend') }}/lib/wow/wow.min.js"></script>
    <script src="{{ asset('ui/frontend') }}/lib/easing/easing.min.js"></script>
    <script src="{{ asset('ui/frontend') }}/lib/waypoints/waypoints.min.js"></script>
    <script src="{{ asset('ui/frontend') }}/lib/owlcarousel/owl.carousel.min.js"></script>

    <!-- Template Javascript -->
    <script src="{{ asset('ui/frontend') }}/js/main.js"></script>
</body>

</html>