<!DOCTYPE html>
<html lang="en">

<x-backend.layouts.partials.header :$title />

<body>
    <!-- Main navbar -->
    <x-backend.layouts.partials.navbar />
    <!-- /main navbar -->


    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        <x-backend.layouts.partials.sidebar />
        <!-- /main sidebar -->


        <!-- Main content -->
        <div class="content-wrapper">
            <!-- Page header -->
            <div class="page-header page-header-light">
                <x-backend.layouts.partials.pageheader :$pageheader />
            </div>
            <!-- /page header -->
            <div class="content">

                <!-- Content area -->
                {{ $slot }}
                <!-- /content area -->

            </div>
            <!-- Footer -->
            <x-backend.layouts.partials.footer />
            <!-- /footer -->
        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->
</body>

</html>
