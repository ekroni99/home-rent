<x-backend.layouts.master>
    <x-slot name="title">Dashoboard</x-slot>
    <x-slot name="pageheader">
        <a href="">Roles</a>
         / Dashoboard
    </x-slot>
    <h1>Dashoboard</h1>
    <table style="width: 100%; border-collapse: collapse" border="1">
        <thead>
            <tr>
                <th colspan="8">
                    <h3 class="text-center">Role</h3>
                </th>
            </tr>
            <tr>
                <th colspan="8" style="text-align: right">
                    <a class="btn btn-primary btn-sm" href="">Add New Role</a>
                </th>
            </tr>
            <tr>
                <th>Sno</th>
                <th>Role Name</th>
                <th class="text-center">Action</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td></td>
                <td></td>
                <td class="text-center">
                    <a class="btn btn-success btn-sm" href="">Show</a>
                    <a class="btn btn-primary btn-sm" href="">Edit</a>
                    <form class="d-inline" action="" method="post">
                        @csrf
                        @method('delete')
                        <button class="btn btn-danger btn-sm" type="submit">Delete</button>
                    </form>
                </td>
            </tr>
        </tbody>
    </table>
</x-backend.layouts.master>
