<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/home', function () {
    return view('backend.roles.home');
});
Route::get('/index', function () {
    return view('backend.roles.index');
});
Route::get('/create', function () {
    return view('backend.roles.create');
});
Route::get('/show', function () {
    return view('backend.roles.show');
});
Route::get('/edit', function () {
    return view('backend.roles.edit');
});

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth', 'verified'])->name('dashboard');

//Route::get('/home',[HomeController::class, 'index'])->middleware('auth')->name('home');
//Route::get('/post',[HomeController::class, 'post'])->middleware(['auth','admin'])->name('post');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__ . '/auth.php';
Route::get('/master',function(){
    return view('components.frontend.layouts.master');
});